# Unofficial understat API.

Setup a local development server and make calls to https://understat.com/.

**Requirements.**
pip, python 3.9

Install virtual env
`pip install virtualenv`

cd into api_task directory `cd understat`

create a venv `virtualenv -p python3.9 venv`

activate the venv `source venv/bin/activate`

install all requirements `pip install -r requirements.txt`

run python `python api.py`

development server only.

http://127.0.0.1:5000/

## API Docs.
### Get competition

**Definition**

- `GET /competitions`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": ["EPL", "La_liga", "Bundesliga", "Serie_A", "Ligue_1", "RFPL"]
    }

```

### Get seasons

**Definition**

- `GET /seasons/<competition>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK", 
        "data": [{"id": "2020", "name": "2020/2021"}, 
                  {"id": "2019", "name": "2019/2020"}, 
                  {"id": "2018", "name": "2018/2019"}, 
                  {"id": "2017", "name": "2017/2018"}, 
                  {"id": "2016", "name": "2016/2017"}, 
                  {"id": "2015", "name": "2015/2016"}, 
                  {"id": "2014", "name": "2014/2015"}]
    }

```

### Get fixtures

**Definition**

- `GET /fixtures/<competition>/<season>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": [{"id": "14086", "isResult": true, "h": {"id": "228", "title": "Fulham", "short_title": "FLH"}, "a": {"id": "83", "title": "Arsenal", "short_title": "ARS"}, "goals": {"h": "0", "a": "3"}, "xG": {"h": "0.126327", "a": "2.16287"}, "datetime": "2020-09-12 11:30:00", "forecast": {"w": "0.0037", "d": "0.0476", "l": "0.9487"}}, 
                  {"id": "14087", "isResult": true, "h": {"id": "78", "title": "Crystal Palace", "short_title": "CRY"}, "a": {"id": "74", "title": "Southampton", "short_title": "SOU"}, "goals": {"h": "1", "a": "0"}, "xG": {"h": "1.39569", "a": "1.26267"}, "datetime": "2020-09-12 14:00:00", "forecast": {"w": "0.3916", "d": "0.3022", "l": "0.3062"}}, 
                  {"id": "14090", "isResult": true, "h": {"id": "87", "title": "Liverpool", "short_title": "LIV"}, "a": {"id": "245", "title": "Leeds", "short_title": "LED"}, "goals": {"h": "4", "a": "3"}, "xG": {"h": "3.15412", "a": "0.269813"}, "datetime": "2020-09-12 16:30:00", "forecast": {"w": "0.9658", "d": "0.0296", "l": "0.0046"}}, 
                  {"id": "14091", "isResult": true, "h": {"id": "81", "title": "West Ham", "short_title": "WHU"}, "a": {"id": "86", "title": "Newcastle United", "short_title": "NEW"}, "goals": {"h": "0", "a": "2"}, "xG": {"h": "0.861445", "a": "1.65911"}, "datetime": "2020-09-12 19:00:00", "forecast": {"w": "0.1506", "d": "0.248", "l": "0.6014"}}, 
                  {"id": "14092", "isResult": true, "h": {"id": "76", "title": "West Bromwich Albion", "short_title": "WBA"}, "a": {"id": "75", "title": "Leicester", "short_title": "LEI"}, "goals": {"h": "0", "a": "3"}, "xG": {"h": "0.352997", "a": "2.95581"}, "datetime": "2020-09-13 13:00:00", "forecast": {"w": "0.007", "d": "0.0358", "l": "0.9572"}}] 
    }

```

### Get match

**Definition**

- `GET /match/<match_id>`

**Response**
- `200 OK` on Success

```json
    {
        "message": "OK",
        "data": "[]"
    }

```