from flask import Flask
from flask_restx import Api, Resource
import understat

app = Flask(__name__)
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'
api = Api(app, version='1.0', title='Unofficial Understat API', description='An unofficial Understat API')


@api.route("/competitions", endpoint='competitions', methods=["GET"])
class GetCompetitions(Resource):
    @staticmethod
    def get():
        """
        Get competitons.

        :return: json
        """
        output = understat.get_competitions()
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/seasons/<competition>", endpoint='seasons', methods=["GET"])
@api.doc(params={'competition': 'A string of the competition'})
class GetSeasons(Resource):
    @staticmethod
    def get(competition):
        """
        Get seasons for a given competiton.

        :param competition: str
        :return: json
        """
        output = understat.get_seasons(competition)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/match/<match_id>", endpoint='match', methods=["GET"])
@api.doc(params={'match_id': 'A string of the match id'})
class GetMatch(Resource):
    @staticmethod
    def get(match_id):
        """
        Get match results for a given match id.

        :param match_id: str
        :return: json
        """
        output = understat.get_match(match_id)
        return {"message": output['message'], "data": output['data']}, output['status_code']


@api.route("/fixtures/<competition>/<season>", endpoint='fixtures', methods=["GET"])
@api.doc(params={'competition': 'A string of the competition', 'season': 'A string of the season'})
class GetFixtures(Resource):
    @staticmethod
    def get(competition, season):
        """
        Get fixtures for a given competition and season.

        :param competition: str
        :param season: int
        :return: json
        """
        output = understat.get_fixtures(competition, season)
        return {"message": output['message'], "data": output['data']}, output['status_code']


if __name__ == '__main__':
    app.run()
