import requests


class UnderStatAPI(object):
    """
    """

    def __init__(self):
        """

        """
        self.url = "https://understat.com/"
        self.headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'}

    def get_competition(self, competition):
        """
        Get competition.

        :param competition: str
        :return: requests obj
        """
        url = f"{self.url}league/{competition}"
        return requests.get(url, headers=self.headers)

    def get_home(self):
        """
        Get home page

        :return: requests obj
        """
        return requests.get(self.url, headers=self.headers)

    def get_match(self, match_id):
        """
        Get match

        :param match_id: int
        :return: requests obj
        """
        url = f"{self.url}match/{match_id}"
        return requests.get(url, headers=self.headers)

    def get_fixtures(self, competition, season=None):
        """
        Get fixtures

        :param competition: str
        :param season: int
        :return: requests obj
        """
        url = f"{self.url}league/{competition}"
        if season:
            url += "/%s" % season
        return requests.get(url, headers=self.headers)
