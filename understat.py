import json
from bs4 import BeautifulSoup
from understat_api import UnderStatAPI

understat_api = UnderStatAPI()


def get_match(match_id):
    """

    :param match_id: int
    :return:
    """
    output = {}
    response = understat_api.get_match(match_id)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    scripts = soup.findAll("script")
    for s in scripts:
        if 'shotsData' in str(s):
            shot_data = str(s).split("JSON.parse('")[1].split("'),")[0]
            shot_data = shot_data.replace("x22", '"')
            shot_data = shot_data.replace("x3A", ':')
            shot_data = shot_data.replace("\\", '')
            shot_data = shot_data.replace("x7D", "}")
            shot_data = shot_data.replace("x5D", "]")
            shot_data = shot_data.replace("x7B", "{")
            shot_data = shot_data.replace("x5B", "[")
            shot_data = shot_data.replace("x2D", "-")
            shot_data = shot_data.replace("x2D", "-")
            shot_data = shot_data.replace("x20", " ")
            output = json.loads(shot_data)

            return {"message": response.reason,
                    "status_code": response.status_code,
                    "data": output}


def get_competitions():
    """

    :return:
    """
    output = []
    response = understat_api.get_home()
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    competitions = soup.find("ul", {"class" :"h-inner"}).findAll('a', {"class": "link"})
    output = [competition['href'].split("/")[-1] for competition in competitions]

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_seasons(competition):
    """
    Get seasons available for competition

    :param competition: str
    :return:
    """
    output = []
    response = understat_api.get_fixtures(competition)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    seasons_div = soup.find("div", {"class": "header-wrapper"}).find("select", {"name": "season"})
    seasons = seasons_div.find_all("option")
    for season in seasons:
        seasons_dict = {"id": season['value'],
                        "name": season.text
                        }
        output.append(seasons_dict)

    return {"message": response.reason,
            "status_code": response.status_code,
            "data": output}


def get_fixtures(competition, season):
    """
    Get competition and season fixtures/results

    :param competition: str
    :param season: int
    :return:
    """
    output = {}
    response = understat_api.get_fixtures(competition, season)
    if response.status_code != 200:
        return {"message": response.reason,
                "status_code": response.status_code,
                "data": output}

    soup = BeautifulSoup(response.text, 'html.parser')
    scripts = soup.findAll("script")
    for s in scripts:
        if 'datesData' in str(s):
            dates_data = str(s).split("JSON.parse('")[1].split("'),")[0]
            dates_data = dates_data.replace("x22", '"')
            dates_data = dates_data.replace("x3A", ':')
            dates_data = dates_data.replace("\\", '')
            dates_data = dates_data.replace("x7D", "}")
            dates_data = dates_data.replace("x5D", "]")
            dates_data = dates_data.replace("x7B", "{")
            dates_data = dates_data.replace("x5B", "[")
            dates_data = dates_data.replace("x2D", "-")
            dates_data = dates_data.replace("x2D", "-")
            dates_data = dates_data.replace("x20", " ")
            output = json.loads(dates_data)

            return {"message": response.reason,
                    "status_code": response.status_code,
                    "data": output}
